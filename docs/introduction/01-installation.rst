.. _local_installation:

Setting up the local working environment
========================================

We will be using the Siesta Mobile, a virtual machine based on the
`Quantum Mobile <https://quantum-mobile.readthedocs.io>`_ sponsored by
the Marvel and MaX projects.

The virtual machine contains an Ubuntu operating system, plus
school-related executables and data files. Users just need to download
the VM image and run it using the VirtualBox framework, which is
available for most operating systems.

For more details about the setup and a link to download the VM image,
please see `this link
<https://drive.google.com/drive/folders/14V50YRuJfW1jxdWkQzZPnTx0TIa10ftX>`_.

Things to add or download/compile later in the VM
-------------------------------------------------

* Working files for the tutorials

  They are currently embedded in the docs, for consistency. One can
  obtain the whole distribution and generate an independent tree of
  working files::

     git clone   https://gitlab.com/garalb/siesta-docs.git
     cd siesta-docs
     cd work-files
     sh link.sh

  This needs to be refined to:

  * Allow creating the working-files hierarchy anywhere.
  * Provide a mechanism to preserve changes made by the students in
    their working directories.
    

  Students should read the docs online
  `here <https://docs.siesta-project.org/projects/siesta>`_, but could also regenerate them
  locally if desired using the above repo. (Check for availability of
  sphinx et al)

* Extra programs

  The programs to process the PDOS/PDOS.xml files can be obtained and
  compiled very easily. See :ref:`this how-to<how-to-dos-pdos>`.


