:sequential_nav: next

..  _tutorial-spin-texture:

Analysis of the spin texture in reciprocal space
================================================

.. note::
   Some more background and context is needed. For further info and
   implementation hints, perhaps link to the OpenMX site.

Background
----------

Implementation in Siesta
------------------------

Mathematically, the spin texture is simply a vector field:
(non-collinear) spins in reciprocal space. In Siesta, the spin vector
associated to a given spinor :math:`\Psi_{nk}` can be computed from
the coefficients of the wave function::

  (equation)

Wavefunction coefficients are stored in a WFSX file, which can be
created during a Siesta calculation in several ways:

* Using the 'COOP.Write T' option: a wave-function set using the
  k-point sampling of the BZ used in the scf cycle will be written
  (see manual).

* Using the 'BandPoints' or 'BandLines' blocks (see manual),
  *together* with the option::
    Wfs.Write.For.Bands T

* Using the WaveFuncKpoints block

In general what one needs depends on the system. Many times simply
having the spin texture along the directions of maximum symmetry, the
same ones used to plot the band structure, is enough. In those
cases BandLines serves perfectly. A regular sampling can be
interesting when one does not know anything about the shape of the band
and is interested in the texture of spin in the whole area of the
Brillouin Zone. It would be the brute force option.

In cases with Dirac cones (graphene, topological insulators), a radial
sampling around the Dirac point (or in concentric circles with
equispaced points) is usually what gives the best information. This is
also the case for the Rashba splitting in  the Au-111 surface (fig).

This is one of the cases where users should know what they need,
there is no generic recipe. 

The selection of states is on the basis of an
eigenvalue interval (the whole range of energies in the file by
default), or by band range.


See the header of the 'read_spin_texture.f90' file for hints about plotting.


Analysis of the spin-texture for a multilayer
---------------------------------------------

Here we will use as an example a system composed of three quintuple
layers of Bi2Se3. 

The initial Siesta calculation uses the `bi2se3_3ql.fdf` file.

We use a 'BandPoints' block to generate the states at chosen k-points
in the basal plane of the BZ, which is effectively two-dimensional in
this case.

Note these keywords are necessary to generate the HSX and WFSX files::

  SaveHS T
  WFS.Write.For.Bands T      

(The 'SaveHS T' option is needed to generate information about the
overlap matrix, which is needed by the analysis program and is not
produced by default for 'bands' calculations).

The following keywords could be used to limit the number of bands stored in
the WFSX file, but care should be taken to specify the interesting
bands in this case (which are they)?::

  ###  Wfs.band.min 1      # Use the right numbers!
  ###  Wfs.band.max 8

The calculation will produce, among other things, the bi2se3_3ql.HSX
and bi2se3_3ql.bands.WFSX files (the latter since it came from a
'bands' calculation). For the purposes of the calculation of the spin
texture, we need to use a name of the form SystemLabel.WFSX::

  ln -sf bi2se3_3ql.bands.WFSX bi2se3_3ql.WFSX

Now we are ready to generate spin-texture information, giving the
'spin_texture' program the SystemLabel string::

  spin_texture bi2se3_3ql > spin_texture.dat

Note that you can use the -m and -M options to further reduce the
energy window. For example, for this system the Fermi level is
around -5 eV (see the output file of the Siesta calculation). Then::

  spin_texture -M 0.0 bi2se3_3ql > spin_texture-0.dat

would eliminate too-high energies. Similarly '-m Min_Energy' would eliminate
energies below Min_Energy. (This widowing is complementary to any use
of the band-filtering discussed above). Moreover energy window could also
be specified in the fdf file for the Siesta calculation (see manual).

.. note::
   The spin_texture executable is not yet in the VM. It will be added
   soon.






