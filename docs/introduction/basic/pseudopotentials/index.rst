:sequential_nav: next

..  _tutorial-basic-pseudopotentials:

Pseudopotentials
================

..  sidebar:: **Have you set up the local environment?**

    If not, :ref:`do that now <local_installation>` before proceeding.

In this exercise we will discuss some basic concepts of
pseudopotentials


.. note::
   Objectives: A closer look at the structure of a .psf file (linked
   to some basic theory), and the KB construction. Things to look for
   in relation to use of a pseudo in Siesta: valence/core split (with semicore
   concept), core corrections (influence on mesh-cutoff). Ghost states
   (examples?). Number of KB projectors (seldom changed from default).
   psf vs. psml pseudos: basics.
   How to *plot* the contents of a .psf or .psml file?
   For more information on actually *generating* a pseudopotential, see
   the advanced tutorial.

   
