:sequential_nav: next

..  _tutorial-basic-output-files:

Output files
============

..  sidebar:: **Have you set up the local environment?**

    If not, :ref:`do that now <local_installation>` before proceeding.

A brief description of the main kinds of output files, with some
indication of which tools are needed to view and/or process their
contents.

It is assumed that the user has seen the :ref:`first encounter
<tutorial-basic-first-encounter>` section.



