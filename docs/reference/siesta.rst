.. _reference_siesta:

SIESTA user manual
==================

The original version in LaTeX of the Siesta manual, with many
convenience macros, is not easily transformed to rst or to html. Hence, we
are for now providing a `link to the pdf version
<https://drive.google.com/file/d/12j4aY51f671hqxtdCVy2Sc4IX0XQSgvy/view?usp=sharing>`_.





