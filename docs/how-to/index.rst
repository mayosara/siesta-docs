.. _how-to:

How-to guides
=============

.. _how-to-build-siesta:

Building Siesta
--------------------------------------------

.. toctree::
    :maxdepth: 1

    conda.rst
    spack.rst

.. _how-to-optimize-basis-sets:

Basis Optimization
------------------------------------

.. toctree::
    :maxdepth: 1

.. _how-to-visualize:

Visualization
-------------

.. toctree::
    :maxdepth: 1

    orbitals.rst
    stm_utils.rst
    dos-pdos.rst
    
