#!/bin/sh

# Proof-of-concept version.
#
# This script scans the 'docs' hierarchy for directories named 'work-files',
# which are supposed to contain the material associated to a given tutorial
# or how-to, and creates a parallel hierarchy with all of them, removing
# the 'work-files' indirection.
#
# The resulting tree can then be copied somewhere else for actual work.
# (Or the script, suitably generalized, called from somewhere else)

# For now, this script has to be executed in the directory 'work-files' (...)
# itself. The location of 'docs' is hardwired.

# Other notes:
#
#  - The copy operations will preserve symbolic links.
#  - It can be argued that the work-files hierarchy should be generated
#  using symbolic links. Still, some intermediate directories have to be
#  created somehow. (See below for hints about this with 'abovepath')
#
destdir=$(pwd)
echo $destdir
#
(cd ../docs;
  for i in $(find . -name 'work-files' ); do
      relpath=${i%/*}   # everything but the final piece
      echo $relpath     # to check the progress
      abovepath=${relpath%/*}
      echo $abovepath
      mkdir -p ${destdir}/$relpath
      # or       mkdir -p ${destdir}/$abovepath
      cp -fRp $relpath/work-files/ ${destdir}/$relpath
      # or ln -s $relpath/work-files ${destdir}/$relpath
  done
)
